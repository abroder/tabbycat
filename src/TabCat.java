import net.abroder.tabcat.controller.TabCatTournamentController;
import net.abroder.tabcat.controller.TabCatTournamentCreatorController;
import net.abroder.tabcat.model.Tournament;
import net.abroder.tabcat.view.TabCatTournamentCreatorView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Created by abroder on 1/24/15.
 */
public class TabCat extends JFrame {
    public TabCat() {
        super("TabCat");

        JMenuBar menuBar = new JMenuBar();
        JMenu file = new JMenu("File");
        JMenuItem newItem = new JMenuItem("New");
        newItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                TabCatTournamentCreatorController creatorController = new TabCatTournamentCreatorController();
            }
        });

        JMenuItem loadItem = new JMenuItem("Load");
        loadItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                TabCatTournamentController tournamentController = new TabCatTournamentController(loadTournament());
            }
        });
        file.add(newItem);
        file.add(loadItem);
        menuBar.add(file);

        setJMenuBar(menuBar);
        setVisible(true);
    }

    // TODO: remove duplicated load code across this and TournamentController
    public Tournament loadTournament() {
        JFileChooser chooser = new JFileChooser();
        chooser.showOpenDialog(getContentPane());

        File loadFile = chooser.getSelectedFile();
        Tournament tournament = null;
        try {
            FileInputStream fin = new FileInputStream(loadFile);
            ObjectInputStream oin = new ObjectInputStream(fin);

            // TODO: put in a warning before load
            tournament = (Tournament) oin.readObject();
            oin.close();
            fin.close();
        } catch (ClassNotFoundException ex) {
            // TODO: proper error message on invalid file
            ex.printStackTrace();
        } catch (IOException ex) {
            // TODO: proper error message on load failure
            ex.printStackTrace();
        }
        return tournament;
    }
}
