package net.abroder.tabcat.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by abroder on 1/5/15.
 */
public class Round implements Serializable {
    private ArrayList<Pairing> pairings;
    private int roundNum;

    public Round(int roundNum, ArrayList<Pairing> pairings) {
        this.roundNum = roundNum;
        this.pairings = pairings;
    }

    public ArrayList<Pairing> getPairings() {
        return pairings;
    }

    @Override
    public String toString() {
        return "Round " +
                roundNum;
    }
}
