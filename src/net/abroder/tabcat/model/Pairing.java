package net.abroder.tabcat.model;

import net.abroder.tabcat.util.TabCatConstants;

import java.io.Serializable;

/**
 * Created by abroder on 1/5/15.
 */
public class Pairing implements TabCatConstants, Serializable {
    private int round;
    private Team pTeam, dTeam;
    private Ballot[] ballots;

    public Pairing(int round, Team pTeam, Team dTeam) {
        this.round = round;
        this.pTeam = pTeam;
        this.dTeam = dTeam;
        this.ballots = new Ballot[NUM_BALLOTS];
        for (int i = 0; i < ballots.length; ++i) {
            this.ballots[i] = new Ballot(this);
        }
    }

    public Team getPTeam() {
        return pTeam;
    }

    public Team getDTeam() {
        return dTeam;
    }

    public Ballot getBallot(int i) {
        return ballots[i];
    }

    public boolean isBallotsEntered() {
        for (int i = 0; i < ballots.length; ++i) {
            if (!ballots[i].isComplete()) {
                return false;
            }
        }
        return true;
    }

    public int getSide(Team t) {
        if (t != pTeam && t != dTeam) return -1;
        return (t == pTeam) ? Ballot.Constants.P_TEAM : Ballot.Constants.D_TEAM;
    }

    public Team getOpponent(Team t) {
        if (t != pTeam && t!= dTeam) return null;
        return (t == pTeam) ? dTeam : pTeam;
    }

    public double getRecord(Team t) {
        if (t != pTeam && t != dTeam) return 0.0;

        double record = 0.0;
        for (int i = 0; i < ballots.length; ++i) {
            record += ballots[i].getRecord(getSide(t));
        }
        return record;
    }

    public int getPointDifferential(Team t) {
        if (t != pTeam && t != dTeam) return 0;

        int pointDifferential = 0;
        for (int i = 0; i < ballots.length; ++i) {
            pointDifferential += ballots[i].getPointDifferential(getSide(t));
        }
        return pointDifferential;
    }

    public void commit() {
        pTeam.setPairing(round, this);
        dTeam.setPairing(round, this);
    }

    @Override
    public String toString() {
        return pTeam +
                " vs " + dTeam;
    }
}
