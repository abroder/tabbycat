package net.abroder.tabcat.model;

import net.abroder.tabcat.util.TabCatConstants;

import java.io.Serializable;

/**
 * Created by abroder on 1/5/15.
 */
public class Team implements TabCatConstants, Serializable {
    private int teamNumber;
    private String schoolName;
    private Character identifier;
    private Pairing[] pairings;

    public Team(int teamNumber, String schoolName) {
        this(teamNumber, schoolName, null);
    }

    public Team(int teamNumber, String schoolName, Character identifier) {
        this.teamNumber = teamNumber;
        this.schoolName = schoolName;
        this.identifier = identifier;
        pairings = new Pairing[TabCatConstants.NUM_ROUNDS];
    }

    public int getTeamNumber() {
        return teamNumber;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public Character getIdentifier() {
        return identifier;
    }

    public Pairing getPairing(int roundNum) {
        return pairings[roundNum - 1];
    }

    public void setPairing(int roundNum, Pairing p) {
        pairings[roundNum - 1] = p;
    }

    public double getRecord(int round) {
        double record = 0.0;
        for (int i = 0; i < round; ++i) {
            record += pairings[i].getRecord(this);
        }
        return record;
    }

    public int getPointDifferential(int round) {
        int pointDifferential = 0;
        for (int i = 0; i < round; ++i) {
            pointDifferential += pairings[i].getPointDifferential(this);
        }
        return pointDifferential;
    }

    public double getChallengeScore(int round) {
        double challengeScore = 0.0;
        for (int i = 0; i < round; ++i) {
            Team opponent = pairings[i].getOpponent(this);
            challengeScore += opponent.getRecord(round);
        }
        return challengeScore;
    }

    @Override
    public String toString() {
        String result = teamNumber +
                " " + schoolName;
        if (identifier != null) {
            result += " " + identifier;
        }

        return result;
    }
}
