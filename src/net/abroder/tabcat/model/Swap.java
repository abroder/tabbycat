package net.abroder.tabcat.model;

/**
 * Created by abroder on 1/24/15.
 */
public class Swap {
    private Pairing lhs, rhs;
    private int side;
    private int round;

    public Swap(Pairing lhs, Pairing rhs, int side, int round) {
        this.lhs = lhs;
        this.rhs = rhs;
        this.side = side;
        this.round = round;
    }

    public Pairing getLhs() {
        return lhs;
    }

    public Pairing getRhs() {
        return rhs;
    }

    public int getSide() {
        return side;
    }

    public double getRecordDifference() {
        if (side == Ballot.Constants.P_TEAM) {
            return Math.abs(lhs.getPTeam().getRecord(round) - rhs.getPTeam().getRecord(round));
        } else {
            return Math.abs(lhs.getDTeam().getRecord(round) - rhs.getDTeam().getRecord(round));
        }
    }

    public double getChallengeScoreDifference() {
        if (side == Ballot.Constants.P_TEAM) {
            return Math.abs(lhs.getPTeam().getChallengeScore(round) - rhs.getPTeam().getChallengeScore(round));
        } else {
            return Math.abs(lhs.getDTeam().getChallengeScore(round) - rhs.getDTeam().getChallengeScore(round));
        }
    }

    public int getPointDifferentialDifference() {
        if (side == Ballot.Constants.P_TEAM) {
            return Math.abs(lhs.getPTeam().getPointDifferential(round) - rhs.getPTeam().getPointDifferential(round));
        } else {
            return Math.abs(lhs.getDTeam().getPointDifferential(round) - rhs.getDTeam().getPointDifferential(round));
        }
    }
}
