package net.abroder.tabcat.model;

import net.abroder.tabcat.util.TabCatConstants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Created by abroder on 1/5/15.
 */
public class Tournament implements TabCatConstants, Serializable {
    private ArrayList<Team> teams;
    private ArrayList<Round> rounds;

    private boolean coinFlip;

    public Tournament(ArrayList<Team> teams) {
        this.teams = teams;
        rounds = new ArrayList<Round>(TabCatConstants.NUM_ROUNDS);
        // TODO: handle round 1 creation in constructor to guarantee consistency
        coinFlip = (new Random()).nextBoolean();
    }

    public ArrayList<Team> getTeams() {
        return teams;
    }

    public Iterator<Round> getRounds() {
        return rounds.iterator();
    }

    public Round getRound(int i) {
        return rounds.get(i-1);
    }

    public int getNumRounds() {
        return rounds.size();
    }

    public void addRound(Round r) {
        rounds.add(r);
    }

    public void setRound(int i, Round r) {
        rounds.set(i-1, r);
    }

    public boolean getCoinFlip() {
        return coinFlip;
    }
}
