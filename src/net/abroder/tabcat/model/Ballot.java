package net.abroder.tabcat.model;

import java.io.Serializable;

/**
 * Created by abroder on 1/6/15.
 */
public class Ballot implements Serializable {
    private Pairing pairing;
    private int[][] scores;

    public Ballot(Pairing pairing) {
        this.pairing = pairing;
        scores = new int[Constants.NUM_TEAMS][Constants.NUM_CATEGORIES];
    }

    public int getScore(int side, int category) {
        return scores[side][category];
    }

    public void setScore(int side, int category, int value) {
        scores[side][category] = value;
    }

    public int getTotal(int side) {
        int total = 0;
        for (int i = Constants.OPENING; i < Constants.NUM_CATEGORIES; ++i) {
            total += scores[side][i];
        }
        return total;
    }

    public Pairing getPairing() {
        return pairing;
    }

    public boolean isComplete() {
        for (int side = Constants.P_TEAM; side < Constants.NUM_TEAMS; ++side) {
            for (int category = Constants.OPENING; category < Constants.NUM_CATEGORIES; ++category) {
                if (scores[side][category] < 1 || scores[side][category] > 10) {
                    return false;
                }
            }
        }
        return true;
    }

    public double getRecord(int side) {
        double pTotal = getTotal(Constants.P_TEAM);
        double dTotal = getTotal(Constants.D_TEAM);

        if (pTotal == dTotal) return 0.5;
        if ((pTotal > dTotal && side == Constants.P_TEAM)
                || (dTotal > pTotal && side == Constants.D_TEAM)) {
            return 1.0;
        }

        return 0.0;
    }

    public int getPointDifferential(int side) {
        int pTotal = getTotal(Constants.P_TEAM);
        int dTotal = getTotal(Constants.D_TEAM);

        return (side == Constants.P_TEAM) ? pTotal - dTotal : dTotal - pTotal;
    }

    public static class Constants {
        public static final int P_TEAM = 0;
        public static final int D_TEAM = 1;
        public static final int NUM_TEAMS = 2;

        public static final int OPENING = 0;

        public static final int DIRECT_ONE_ATTORNEY = 1;
        public static final int DIRECT_ONE_WITNESS = 2;
        public static final int CROSS_ONE_WITNESS = 3;

        public static final int DIRECT_TWO_ATTORNEY = 4;
        public static final int DIRECT_TWO_WITNESS = 5;
        public static final int CROSS_TWO_WITNESS = 6;

        public static final int DIRECT_THREE_ATTORNEY = 7;
        public static final int DIRECT_THREE_WITNESS = 8;
        public static final int CROSS_THREE_WITNESS = 9;

        public static final int CROSS_ONE_ATTORNEY = 10;
        public static final int CROSS_TWO_ATTORNEY = 11;
        public static final int CROSS_THREE_ATTORNEY = 12;

        public static final int CLOSING = 13;

        public static final int NUM_CATEGORIES = 14;
    }
}
