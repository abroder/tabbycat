package net.abroder.tabcat.view;

import net.abroder.tabcat.controller.TabCatTournamentController;
import net.abroder.tabcat.model.Round;
import net.abroder.tabcat.model.Tournament;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Iterator;

/**
 * Created by abroder on 1/5/15.
 */
public class TabCatTournamentView extends JPanel {
    private TabCatTournamentController controller;
    private JComboBox roundSelector;
    private JList pairingSelector;
    private JButton ballotsButton;
    private JButton detailsButton;
    private JButton saveButton;
    private JButton loadButton;
    private JButton pairButton;

    public TabCatTournamentView(TabCatTournamentController controller) {
        this.controller = controller;
        setLayout(new BorderLayout());
    }

    public void drawTournament(Tournament tournament, int currentRound) {
        removeAll();

        drawRoundSelector(tournament, currentRound);
        drawRoundView(tournament, currentRound);
        drawPairingButtons();
        drawCreatePairsButton();
    }

    private void drawRoundSelector(Tournament tournament, int currentRound) {
        roundSelector = new JComboBox();

        Iterator<Round> roundIterator = tournament.getRounds();
        while (roundIterator.hasNext()) {
            Round r = roundIterator.next();
            roundSelector.addItem(r);
        }
        roundSelector.setSelectedIndex(currentRound - 1);

        roundSelector.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                if (itemEvent.getStateChange() == ItemEvent.SELECTED) {
                    controller.selectRound(roundSelector.getSelectedIndex());
                }
            }
        });

        add(roundSelector, BorderLayout.NORTH);
    }

    private void drawRoundView(Tournament tournament, int currentRound) {
        final Round selectedRound = tournament.getRound(currentRound);

        pairingSelector = new JList();
        pairingSelector.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        pairingSelector.setListData(selectedRound.getPairings().toArray());
        pairingSelector.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                int selectedPairing = pairingSelector.getSelectedIndex();
                ballotsButton.setEnabled(selectedPairing != -1);
                detailsButton.setEnabled(selectedPairing != -1 && selectedRound.getPairings().get(selectedPairing).isBallotsEntered());
            }
        });

        JScrollPane pairingScrollPane = new JScrollPane(pairingSelector);
        add(pairingScrollPane, BorderLayout.CENTER);
    }

    private void drawPairingButtons() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        ballotsButton = new JButton("Ballots");
        ballotsButton.setEnabled(false);
        ballotsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                controller.editBallots(roundSelector.getSelectedIndex(), pairingSelector.getSelectedIndex());
            }
        });
        panel.add(ballotsButton);


        detailsButton = new JButton("Details");
        detailsButton.setEnabled(false);
        detailsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                controller.showDetails(roundSelector.getSelectedIndex(), pairingSelector.getSelectedIndex());
            }
        });
        panel.add(detailsButton);

        saveButton = new JButton("Save");
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                controller.saveTournament();
            }
        });
        panel.add(saveButton);

        loadButton = new JButton("Load");
        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                controller.loadTournament();
            }
        });
        panel.add(loadButton);

        add(panel, BorderLayout.EAST);
    }

    private void drawCreatePairsButton() {
        pairButton = new JButton("Pair");

        // TODO: only enable pairing button when ready to pair
//        pairButton.setEnabled(false);
        pairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                controller.pairRound(roundSelector.getSelectedIndex());
            }
        });
        add(pairButton, BorderLayout.SOUTH);
    }
}
