package net.abroder.tabcat.view;

import net.abroder.tabcat.controller.TabCatBallotController;
import net.abroder.tabcat.model.Ballot;
import net.abroder.tabcat.model.Pairing;
import net.abroder.tabcat.util.NumericFilter;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * Created by abroder on 12/19/14.
 */
public class TabCatBallotView extends JPanel {
    private TabCatBallotController controller;

    private JPanel panel;

    private JLabel pTeamLabel;
    private JLabel dTeamLabel;

    private JLabel pTotal;
    private JLabel dTotal;

    private JTextField pOpeningField;
    private JTextField pDirect1AttorneyField;
    private JTextField pDirect1WitnessField;
    private JTextField pCross1WitnessField;
    private JTextField pDirect2AttorneyField;
    private JTextField pDirect2WitnessField;
    private JTextField pCross2WitnessField;
    private JTextField pDirect3AttorneyField;
    private JTextField pDirect3WitnessField;
    private JTextField pCross3WitnessField;
    private JTextField pCross1AttorneyField;
    private JTextField pCross2AttorneyField;
    private JTextField pCross3AttorneyField;
    private JTextField pClosingField;

    private JTextField dOpeningField;
    private JTextField dCross1AttorneyField;
    private JTextField dCross2AttorneyField;
    private JTextField dCross3AttorneyField;
    private JTextField dDirect1AttorneyField;
    private JTextField dDirect1WitnessField;
    private JTextField dCross1WitnessField;
    private JTextField dDirect2AttorneyField;
    private JTextField dDirect2WitnessField;
    private JTextField dCross2WitnessField;
    private JTextField dDirect3AttorneyField;
    private JTextField dDirect3WitnessField;
    private JTextField dCross3WitnessField;
    private JTextField dClosingField;

    private JButton doneButton;

    private FocusListener focusListener;

    private JTextField[][] fields = {
            {
                    pOpeningField,
                    pDirect1AttorneyField, pDirect1WitnessField, pCross1WitnessField,
                    pDirect2AttorneyField, pDirect2WitnessField, pCross2WitnessField,
                    pDirect3AttorneyField, pDirect3WitnessField, pCross3WitnessField,
                    pCross1AttorneyField, pCross2AttorneyField, pCross3AttorneyField,
                    pClosingField
            },
            {
                    dOpeningField,
                    dDirect1AttorneyField, dDirect1WitnessField, dCross1WitnessField,
                    dDirect2AttorneyField, dDirect2WitnessField, dCross2WitnessField,
                    dDirect3AttorneyField, dDirect3WitnessField, dCross3WitnessField,
                    dCross1AttorneyField, dCross2AttorneyField, dCross3AttorneyField,
                    dClosingField
            }
    };

    private static final String DOCUMENT_SIDE = "side";
    private static final String DOCUMENT_CATEGORY = "category";

    public TabCatBallotView(TabCatBallotController controller) {
        super();

        add(panel);
        this.controller = controller;

        createFieldProperties();
        createFocusListener();
        addFocusListener();
        addNumericFilter();
        addDoneListener();
    }

    private void addDoneListener() {
        doneButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                controller.donePressed();
            }
        });
    }

    private void addNumericFilter() {
        for (int side = Ballot.Constants.P_TEAM; side < Ballot.Constants.NUM_TEAMS; ++side) {
            for (int category = Ballot.Constants.OPENING; category < Ballot.Constants.NUM_CATEGORIES; ++category) {
                ((AbstractDocument) fields[side][category].getDocument()).setDocumentFilter(new NumericFilter());
            }
        }
    }

    private void createFocusListener() {
        focusListener = new FocusListener() {
            @Override
            public void focusGained(FocusEvent focusEvent) { }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                JTextField field = (JTextField) focusEvent.getSource();
                if (field.getText().length() == 0 || Integer.parseInt(field.getText()) < 1 || Integer.parseInt(field.getText()) > 10) {
                    if (field.getText().length() == 0) {
                        field.setText(String.valueOf(0));
                    }
                    field.setForeground(Color.RED);
                    return;
                }

                Document fieldDocument = field.getDocument();

                controller.scoreUpdated((Integer) fieldDocument.getProperty(DOCUMENT_SIDE),
                        (Integer) fieldDocument.getProperty(DOCUMENT_CATEGORY),
                        Integer.parseInt(field.getText()));
            }
        };
    }

    private void createFieldProperties() {
        for (int side = Ballot.Constants.P_TEAM; side < Ballot.Constants.NUM_TEAMS; ++side) {
            for (int category = Ballot.Constants.OPENING; category < Ballot.Constants.NUM_CATEGORIES; ++category) {
                Document fieldDocument = fields[side][category].getDocument();
                fieldDocument.putProperty(DOCUMENT_SIDE, side);
                fieldDocument.putProperty(DOCUMENT_CATEGORY, category);
            }
        }
    }

    private void addFocusListener() {
        for (int side = Ballot.Constants.P_TEAM; side < Ballot.Constants.NUM_TEAMS; ++side) {
            for (int category = Ballot.Constants.OPENING; category < Ballot.Constants.NUM_CATEGORIES; ++category) {
                fields[side][category].addFocusListener(focusListener);
            }
        }
    }

    public void drawBallot(Ballot ballot) {
        drawPairing(ballot);
        drawScores(ballot);
        drawTotal(ballot);
    }

    private void drawPairing(Ballot ballot) {
        Pairing pairing = ballot.getPairing();
        pTeamLabel.setText(pairing.getPTeam().toString());
        dTeamLabel.setText(pairing.getDTeam().toString());
    }

    private void drawScores(Ballot ballot) {
        for (int side = Ballot.Constants.P_TEAM; side < Ballot.Constants.NUM_TEAMS; ++side) {
            for (int category = Ballot.Constants.OPENING; category < Ballot.Constants.NUM_CATEGORIES; ++category) {
                fields[side][category].setText(String.valueOf(ballot.getScore(side, category)));
            }
        }
    }

    public void drawTotal(Ballot ballot) {
        pTotal.setText(String.valueOf(ballot.getTotal(Ballot.Constants.P_TEAM)));
        dTotal.setText(String.valueOf(ballot.getTotal(Ballot.Constants.D_TEAM)));
    }
}
