package net.abroder.tabcat.view;

import net.abroder.tabcat.controller.TabCatTournamentCreatorController;
import net.abroder.tabcat.model.Team;
import net.abroder.tabcat.util.NumericFilter;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by abroder on 1/23/15.
 */
public class TabCatTournamentCreatorView extends JPanel {
    private TabCatTournamentCreatorController controller;
    private ArrayList<JTextField> fields;

    public TabCatTournamentCreatorView(TabCatTournamentCreatorController controller) {
        super();
        this.controller = controller;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        fields = new ArrayList<JTextField>();
    }

    public void drawTournamentCreator(int numTeams) {
        createHeader();
        createTeamFields();

        JButton startButton = new JButton("Done");
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (inputValid()) {
                    controller.createPairings(collectTeams());
                }
            }
        });
        startButton.setAlignmentX(0.5f);
        add(startButton);
    }

    private void createHeader() {
        JPanel headerPanel = new JPanel();
        headerPanel.setLayout(new GridLayout());
        JLabel pLabel = new JLabel("π");
        pLabel.setHorizontalAlignment(JLabel.CENTER);
        headerPanel.add(pLabel);
        JLabel dLabel = new JLabel("∆");
        dLabel.setHorizontalAlignment(JLabel.CENTER);
        headerPanel.add(dLabel);

        add(headerPanel);
    }

    private void createTeamFields() {
        // TODO: remove hardcoding of team count
        NumericFilter filter = new NumericFilter();
        for (int i = 0; i < 10; ++i) {
            JPanel panel = new JPanel();

            JPanel plaintiffPanel = new JPanel();
            JTextField pTeamNumField = new JTextField(4);
            ((AbstractDocument) pTeamNumField.getDocument()).setDocumentFilter(filter);
            JTextField plaintiffField = new JTextField(15);
            JTextField plaintiffLetterField = new JTextField(1);
            plaintiffPanel.add(pTeamNumField);
            plaintiffPanel.add(plaintiffField);
            plaintiffPanel.add(plaintiffLetterField);

            JPanel defensePanel = new JPanel();
            JTextField dTeamNumField = new JTextField(4);
            ((AbstractDocument) dTeamNumField.getDocument()).setDocumentFilter(filter);
            JTextField defenseField = new JTextField(15);
            JTextField dLetterField = new JTextField(1);
            defensePanel.add(dTeamNumField);
            defensePanel.add(defenseField);
            defensePanel.add(dLetterField);

            panel.add(plaintiffPanel);
            panel.add(defensePanel);

            fields.add(pTeamNumField);
            fields.add(plaintiffField);
            fields.add(plaintiffLetterField);
            fields.add(dTeamNumField);
            fields.add(defenseField);
            fields.add(dLetterField);

            add(panel);
        }
    }

    private boolean inputValid() {
        for (int i = 0; i < fields.size(); ++i) {
            if ((i + 1) % 3 != 0 && fields.get(i).getText().length() == 0) return false;
        }
        return true;
    }

    private ArrayList<Team> collectTeams() {
        ArrayList<Team> teams = new ArrayList<Team>();
        for (int i = 0; i < fields.size(); i += 3) {
            int num = Integer.parseInt(fields.get(i).getText());
            String school = fields.get(i + 1).getText();
            if (fields.get(i + 2).getText().length() > 0) {
                teams.add(new Team(num, school, fields.get(i + 2).getText().charAt(0)));
            } else {
                teams.add(new Team(num, school));
            }
        }
        return teams;
    }
}
