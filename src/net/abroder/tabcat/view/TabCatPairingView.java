package net.abroder.tabcat.view;

import net.abroder.tabcat.controller.TabCatPairingController;
import net.abroder.tabcat.model.Pairing;

import javax.swing.*;

/**
 * Created by abroder on 1/12/15.
 */
public class TabCatPairingView extends JPanel {
    private TabCatPairingController controller;

    public TabCatPairingView(TabCatPairingController controller) {
        this.controller = controller;
    }

    public void drawPairing(Pairing pairing, int round) {
        // TODO: make prettier

        add(new JLabel("π: " + pairing.getPTeam().toString()));
        drawStat(pairing.getPTeam().getRecord(round), "Record");
        if (round > 1) {
            drawStat(pairing.getPTeam().getChallengeScore(round), "Running CS");
        }
        drawStat(pairing.getPTeam().getPointDifferential(round), "Running PD");

        add(new JLabel("Δ: " + pairing.getDTeam().toString()));
        drawStat(pairing.getDTeam().getRecord(round), "Record");
        if (round > 1) {
            drawStat(pairing.getDTeam().getChallengeScore(round), "Running CS");
        }
        drawStat(pairing.getDTeam().getPointDifferential(round), "Running PD");
    }

    public void drawStat(double statValue, String name) {
        JPanel statPanel = new JPanel();
        JLabel statLabel = new JLabel(name + ": ");
        JLabel stat = new JLabel(Double.toString(statValue));
        statPanel.add(statLabel);
        statPanel.add(stat);
        add(statPanel);
    }
}
