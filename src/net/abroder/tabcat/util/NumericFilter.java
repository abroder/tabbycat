package net.abroder.tabcat.util;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 * Created by abroder on 1/9/15.
 */
public class NumericFilter extends DocumentFilter {
    @Override
    public void insertString(FilterBypass filterBypass, int off, String s, AttributeSet attributeSet) throws BadLocationException {
        filterBypass.insertString(off, s.replaceAll("\\D++", ""), attributeSet);
    }

    @Override
    public void replace(FilterBypass filterBypass, int off, int len, String s, AttributeSet attributeSet) throws BadLocationException {
        filterBypass.replace(off, len, s.replaceAll("\\D++", ""), attributeSet);
    }
}
