package net.abroder.tabcat.util;

/**
 * Created by abroder on 1/5/15.
 */
public interface TabCatConstants {
    public static final int NUM_ROUNDS = 4;
    public static final int NUM_BALLOTS = 2;
}
