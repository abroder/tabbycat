package net.abroder.tabcat.controller;

import net.abroder.tabcat.model.Pairing;
import net.abroder.tabcat.view.TabCatPairingView;

import javax.swing.*;

/**
 * Created by abroder on 1/12/15.
 */
public class TabCatPairingController {
    private Pairing pairing;
    private int round;

    private TabCatPairingView view;

    public TabCatPairingController(Pairing pairing, int round) {
        this.pairing = pairing;

        JDialog frame = new JDialog();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setModalityType(JDialog.ModalityType.APPLICATION_MODAL);

        view = new TabCatPairingView(this);
        view.drawPairing(this.pairing, round);

        frame.getContentPane().add(view);
        frame.setVisible(true);
        frame.pack();
    }

    public void setPairing(Pairing pairing) {
        this.pairing = pairing;
        view.drawPairing(this.pairing, round);
    }
}
