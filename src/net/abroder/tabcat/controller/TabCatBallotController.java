package net.abroder.tabcat.controller;

import net.abroder.tabcat.model.Ballot;
import net.abroder.tabcat.view.TabCatBallotView;

import javax.swing.*;

/**
 * Created by abroder on 1/9/15.
 */
public class TabCatBallotController {
    private Ballot ballot;

    private JDialog dialog;
    private TabCatBallotView view;

    public TabCatBallotController(Ballot ballot) {
        this.ballot = ballot;
        this.view = new TabCatBallotView(this);

        this.view.setVisible(true);

        dialog = new JDialog();
        dialog.setModalityType(JDialog.ModalityType.APPLICATION_MODAL);
        dialog.getContentPane().add(view);
        view.drawBallot(ballot);
        dialog.pack();
        dialog.setVisible(true);
    }

    public void donePressed() {
        dialog.dispose();
    }

    public void scoreUpdated(int side, int category, int value) {
        if (value != ballot.getScore(side, category)) {
            ballot.setScore(side, category, value);
            view.drawTotal(ballot);
        }
    }
}
