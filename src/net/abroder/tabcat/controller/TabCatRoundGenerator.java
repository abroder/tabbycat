package net.abroder.tabcat.controller;

import net.abroder.tabcat.model.*;
import net.abroder.tabcat.util.TabCatConstants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by abroder on 1/23/15.
 */
public class TabCatRoundGenerator implements TabCatConstants {
    private Tournament tournament;
    private int roundNumber;

    public TabCatRoundGenerator(Tournament tournament, int roundNumber) {
        this.tournament = tournament;
        this.roundNumber = roundNumber;
    }

    public Round generateRound() {
        if (roundNumber == 2 || roundNumber == 4) {
            return generateRoundBySides();
        } else {
            return generateRoundFromAll();
        }
    }

    /* TODO: unify round generation
     The only things that need to be different between rounds 2&4 are the
     generation of the tentative pairings, and finding potential swaps.
     Everything else can be unified across the rounds.
     */
    private Round generateRoundFromAll() {
        ArrayList<Team> teams = (ArrayList<Team>) tournament.getTeams().clone();
        Collections.sort(teams, new TeamComparator());

        ArrayList<Pairing> tentativePairings = new ArrayList<Pairing>();
        for (int i = 0; i < teams.size(); i += 2) {
            Pairing p = new Pairing(roundNumber, teams.get(i), teams.get(i + 1));
            tentativePairings.add(p);
        }

        for (int i = 0; i < tentativePairings.size(); ++i) {
            Pairing pairing = tentativePairings.get(i);
            if (!permissibleMatch(pairing)) {
                Swap bestSwap = findBestSwapOdd(pairing, tentativePairings, teams, i);

                Pairing newPairingOne = new Pairing(roundNumber, bestSwap.getLhs().getPTeam(), bestSwap.getRhs().getDTeam());
                Pairing newPairingTwo = new Pairing(roundNumber, bestSwap.getRhs().getPTeam(), bestSwap.getLhs().getDTeam());

                int lhsIndex = tentativePairings.indexOf(bestSwap.getLhs());
                int rhsIndex = tentativePairings.indexOf(bestSwap.getRhs());

                tentativePairings.set(lhsIndex, newPairingOne);
                tentativePairings.set(rhsIndex, newPairingTwo);
            }
        }

        for (int i = 0; i < teams.size() / 2; ++i) {
            tentativePairings.get(i).commit();
        }

        return new Round(roundNumber, tentativePairings);
    }

    private Round generateRoundBySides() {
        ArrayList<Team> pTeams = new ArrayList<Team>();
        ArrayList<Team> dTeams = new ArrayList<Team>();

        for (int i = 0; i < tournament.getTeams().size(); ++i) {
            Team team = tournament.getTeams().get(i);
            if (team.getPairing(roundNumber - 1).getSide(team) == Ballot.Constants.P_TEAM) {
                dTeams.add(team);
            } else {
                pTeams.add(team);
            }
        }

        Collections.sort(pTeams, new TeamComparator());
        Collections.sort(dTeams, new TeamComparator());

        ArrayList<Pairing> tentativePairings = new ArrayList<Pairing>();
        for (int i = 0; i < pTeams.size(); ++i) {
            Pairing p = new Pairing(roundNumber, pTeams.get(i), dTeams.get(i));
            tentativePairings.add(p);
        }

        for (int i = 0; i < tentativePairings.size(); ++i) {
            Pairing pairing = tentativePairings.get(i);
            if (!permissibleMatch(pairing)) {
                Swap bestSwap = findBestSwapEven(pairing, tentativePairings, i);
                Pairing newPairingOne = new Pairing(roundNumber, bestSwap.getLhs().getPTeam(), bestSwap.getRhs().getDTeam());
                Pairing newPairingTwo = new Pairing(roundNumber, bestSwap.getRhs().getPTeam(), bestSwap.getLhs().getDTeam());

                int lhsIndex = tentativePairings.indexOf(bestSwap.getLhs());
                int rhsIndex = tentativePairings.indexOf(bestSwap.getRhs());

                tentativePairings.set(lhsIndex, newPairingOne);
                tentativePairings.set(rhsIndex, newPairingTwo);
            }
        }

        for (int i = 0; i < pTeams.size(); ++i) {
            tentativePairings.get(i).commit();
        }

        return new Round(roundNumber, tentativePairings);
    }

    private Swap findBestSwapOdd(Pairing pairing, ArrayList<Pairing> tentativePairings, ArrayList<Team> rankedTeams, int i) {
        Swap bestSwap = null;
        int difference = 1;

        while (bestSwap == null) {
            ArrayList<Swap> potentialSwaps = new ArrayList<Swap>();

            // TODO: Create potential swaps

            // Find least difference swap
            double leastRecordDifference = Double.MAX_VALUE;
            for (int j = 0; j < potentialSwaps.size(); ++j) {
                if (potentialSwaps.get(j).getRecordDifference() == leastRecordDifference) {
                    bestSwap = null;
                } else if (potentialSwaps.get(j).getRecordDifference() < leastRecordDifference) {
                    leastRecordDifference = potentialSwaps.get(j).getRecordDifference();
                    bestSwap = potentialSwaps.get(j);
                }
            }
            if (bestSwap != null) {
                return bestSwap;
            }

            // TODO: add CS difference to rounds 3 and 4 pairing
            if (roundNumber >= 3) {

            }

            int leastPDDifference = Integer.MAX_VALUE;
            for (int j = 0; j < potentialSwaps.size(); ++j) {
                if (potentialSwaps.get(j).getRecordDifference() != leastRecordDifference) {
                    continue;
                }

                if (potentialSwaps.get(j).getPointDifferentialDifference() == leastPDDifference) {
                    bestSwap = null;
                } else if (potentialSwaps.get(j).getPointDifferentialDifference() < leastPDDifference) {
                    leastPDDifference = potentialSwaps.get(j).getPointDifferentialDifference();
                    bestSwap = potentialSwaps.get(j);
                }
            }
            if (bestSwap != null) {
                return bestSwap;
            }

            difference++;
        }

        return null;
    }

    private Swap findBestSwapEven(Pairing pairing, ArrayList<Pairing> tentativePairings, int i) {
        Swap bestSwap = null;
        int difference = 1;

        while (bestSwap == null) {
            ArrayList<Swap> potentialSwaps = new ArrayList<Swap>();

            // Create potential swaps
            if (i - difference >= 0) {
                potentialSwaps.add(new Swap(pairing, tentativePairings.get(i - difference), Ballot.Constants.P_TEAM, roundNumber));
                potentialSwaps.add(new Swap(pairing, tentativePairings.get(i - difference), Ballot.Constants.D_TEAM, roundNumber));
            }
            if (i + difference < tentativePairings.size()) {
                potentialSwaps.add(new Swap(pairing, tentativePairings.get(i + difference), Ballot.Constants.P_TEAM, roundNumber));
                potentialSwaps.add(new Swap(pairing, tentativePairings.get(i + difference), Ballot.Constants.D_TEAM, roundNumber));
            }

            // Find least difference swap
            double leastRecordDifference = Double.MAX_VALUE;
            for (int j = 0; j < potentialSwaps.size(); ++j) {
                if (potentialSwaps.get(j).getRecordDifference() == leastRecordDifference) {
                    bestSwap = null;
                } else if (potentialSwaps.get(j).getRecordDifference() < leastRecordDifference) {
                    leastRecordDifference = potentialSwaps.get(j).getRecordDifference();
                    bestSwap = potentialSwaps.get(j);
                }
            }
            if (bestSwap != null) {
                return bestSwap;
            }

            // TODO: add CS difference to rounds 3 and 4 pairing
            if (roundNumber >= 3) {

            }

            int leastPDDifference = Integer.MAX_VALUE;
            for (int j = 0; j < potentialSwaps.size(); ++j) {
                if (potentialSwaps.get(j).getRecordDifference() != leastRecordDifference) {
                    continue;
                }

                if (potentialSwaps.get(j).getPointDifferentialDifference() == leastPDDifference) {
                    bestSwap = null;
                } else if (potentialSwaps.get(j).getPointDifferentialDifference() < leastPDDifference) {
                    leastPDDifference = potentialSwaps.get(j).getPointDifferentialDifference();
                    bestSwap = potentialSwaps.get(j);
                }
            }
            if (bestSwap != null) {
                return bestSwap;
            }

            difference++;
        }

        return null;
    }

    private boolean permissibleMatch(Pairing pairing) {
            /* Teams from the same school can not hit each other */
        if (pairing.getPTeam().getSchoolName().equals(pairing.getDTeam().getSchoolName())) {
            return false;
        }

            /* Teams can not hit each other more than once at the same tournament */
        Team pTeam = pairing.getPTeam();
        for (int i = 1; i < roundNumber; ++i) {
            Pairing oldPairing = pTeam.getPairing(i);
            if (oldPairing.getOpponent(pTeam) == pairing.getDTeam()) {
                return false;
            }
        }

        return true;
    }

    private class TeamComparator implements Comparator<Team> {
        @Override
        public int compare(Team lhs, Team rhs) {
            if (lhs.getTeamNumber() == rhs.getTeamNumber()) return 0;

            /* First tiebreaker: record */
            if (lhs.getRecord(roundNumber - 1) > rhs.getRecord(roundNumber - 1)) {
                return -1;
            } else if (lhs.getRecord(roundNumber - 1) < rhs.getRecord(roundNumber - 1)) {
                return 1;
            }

            /* Second tiebreaker: challenge score.
             * Ignored for Round 2 pairings */
            if (roundNumber > 2) {
                if (lhs.getChallengeScore(roundNumber - 1) > rhs.getChallengeScore(roundNumber - 1)) {
                    return -1;
                } else if (lhs.getChallengeScore(roundNumber - 1) < rhs.getChallengeScore(roundNumber - 1)) {
                    return 1;
                }
            }

            /* Second tiebreaker: point differential */
            if (lhs.getPointDifferential(roundNumber - 1) > rhs.getPointDifferential(roundNumber - 1)) {
                return -1;
            } else if (lhs.getPointDifferential(roundNumber - 1) < rhs.getPointDifferential(roundNumber - 1)) {
                return 1;
            }

            /* Final tiebreaker: team number, based on coin flip
            * if heads, the larger number wins. if tails, the smaller number wins */
            if (tournament.getCoinFlip()) {
                return (lhs.getTeamNumber() > rhs.getTeamNumber()) ? -1 : 1;
            } else {
                return (lhs.getTeamNumber() > rhs.getTeamNumber()) ? 1 : -1;
            }
        }
    }
}
