package net.abroder.tabcat.controller;

import net.abroder.tabcat.model.Pairing;
import net.abroder.tabcat.model.Round;
import net.abroder.tabcat.model.Team;
import net.abroder.tabcat.model.Tournament;
import net.abroder.tabcat.view.TabCatTournamentCreatorView;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by abroder on 1/23/15.
 */
public class TabCatTournamentCreatorController {
    private JFrame frame;
    private TabCatTournamentCreatorView view;

    public TabCatTournamentCreatorController() {
        frame = new JFrame();

        view = new TabCatTournamentCreatorView(this);
        view.drawTournamentCreator(20);

        frame.getContentPane().add(view);
        frame.setVisible(true);
        frame.pack();
    }

    public void createPairings(ArrayList<Team> teams) {
        Tournament tournament = new Tournament(teams);
        ArrayList<Pairing> pairings = new ArrayList<Pairing>();
        for (int i = 0; i < teams.size(); i += 2) {
            Pairing p = new Pairing(1, teams.get(i), teams.get(i + 1));
            p.commit();
            pairings.add(p);
        }
        Round round = new Round(1, pairings);
        tournament.addRound(round);

        frame.setVisible(false);
        TabCatTournamentController tournamentController = new TabCatTournamentController(tournament);
    }
}
