package net.abroder.tabcat.controller;

import net.abroder.tabcat.model.Pairing;
import net.abroder.tabcat.model.Round;
import net.abroder.tabcat.model.Tournament;
import net.abroder.tabcat.view.TabCatTournamentView;

import javax.swing.*;
import java.io.*;

/**
 * Created by abroder on 1/5/15.
 */
public class TabCatTournamentController {
    private Tournament mTournament;

    private TabCatTournamentView view;

    public TabCatTournamentController(Tournament tournament) {
        mTournament = tournament;

        JFrame frame = new JFrame("TabCat");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        view = new TabCatTournamentView(this);
        view.drawTournament(tournament, tournament.getNumRounds());

        frame.getContentPane().add(view);
        frame.setVisible(true);
        frame.pack();
    }

    public void setTournament(Tournament tournament) {
        mTournament = tournament;
        view.drawTournament(mTournament, 1);
    }

    public void selectRound(int selectedRoundIndex) {
        view.drawTournament(mTournament, selectedRoundIndex+1);
    }

    public void editBallots(int selectedRoundIndex, int selectedPairingIndex) {
        Round selectedRound = mTournament.getRound(selectedRoundIndex + 1);
        Pairing selectedPairing = selectedRound.getPairings().get(selectedPairingIndex);

        for (int i = 0; i < 2; ++i) {
            TabCatBallotController ballotController = new TabCatBallotController(selectedPairing.getBallot(i));
        }

        // TODO: check to activate pairing button
    }

    public void showDetails(int selectedRoundIndex, int selectedPairingIndex) {
        Round selectedRound = mTournament.getRound(selectedRoundIndex + 1);
        Pairing selectedPairing = selectedRound.getPairings().get(selectedPairingIndex);

        TabCatPairingController detailsController = new TabCatPairingController(selectedPairing, selectedRoundIndex + 1);
    }

    public void pairRound(int selectedRoundIndex) {
        TabCatRoundGenerator roundGenerator = new TabCatRoundGenerator(mTournament, selectedRoundIndex + 2);
        Round newRound = roundGenerator.generateRound();

        // TODO: make round adding more consistent and robust
        try {
            mTournament.setRound(selectedRoundIndex + 2, newRound);
        } catch (Exception ex) {
            mTournament.addRound(newRound);
        }
    }

    public void saveTournament() {
        JFileChooser chooser = new JFileChooser();
        chooser.showSaveDialog(view);

        File saveFile = chooser.getSelectedFile();
        try {
            FileOutputStream fout = new FileOutputStream(saveFile);
            ObjectOutputStream oout = new ObjectOutputStream(fout);
            oout.writeObject(mTournament);
            oout.close();
            fout.close();
        } catch (IOException ex) {
            // TODO: proper error message on save failure
            ex.printStackTrace();
        }
    }

    public void loadTournament() {
        JFileChooser chooser = new JFileChooser();
        chooser.showOpenDialog(view);

        File loadFile = chooser.getSelectedFile();
        try {
            FileInputStream fin = new FileInputStream(loadFile);
            ObjectInputStream oin = new ObjectInputStream(fin);

            // TODO: put in a warning before load
            setTournament((Tournament) oin.readObject());
            oin.close();
            fin.close();
        } catch (ClassNotFoundException ex) {
            // TODO: proper error message on invalid file
            ex.printStackTrace();
        } catch (IOException ex) {
            // TODO: proper error message on load failure
            ex.printStackTrace();
        }
    }
}
