import net.abroder.tabcat.controller.TabCatTournamentController;
import net.abroder.tabcat.controller.TabCatTournamentCreatorController;
import net.abroder.tabcat.model.Tournament;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        try {
            System.setProperty("apple.laf.useScreenMenuBar", "true");
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Test");
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
        }

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new TabCat();
            }
        });
    }
}